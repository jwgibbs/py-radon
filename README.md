Python/Cython code for reconstructing computed tomography (CT) data. It has three functions:

* radon.filter for doing a high-pass filter to the sinogram data
* radon.iradon for back projecting the filtered sinogram data
* radon.radon for forward projecting real space data

Installation
============

The code is setup to try using OpenMP so try to use a compiler that supports it.  On my machine, that looks like:

    export CC=/usr/local/bin/gcc-4.8
    python setup.py build
    python setup.py install

Notes
=====

I use this code extensively and I'm pretty sure it does the calculations correctly. That said, I haven't really worked on making the interface intuitive or documenting it.  Also, I've only tested this on two machines, both of which are Macs...