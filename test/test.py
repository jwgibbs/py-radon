#! python
import numpy as np
import matplotlib.pyplot as plot
import radon
import im3D

fn = "sin_act.tif"

print('Loading the sinogram')
sin_act = im3D.io.read_tiff(fn).astype(np.float64)
sin_act *= 0.5**16
sin_act = -np.log(sin_act)

print('Filtering the sinogram')
sin_err = radon.filter(sin_act, constant=0.0, ramp=1.0, parz=0.2)

print('Back projecting the sinogram')
rot_ctr = radon.find_rotctr(sin_err, multi_res=3, span=5, quad_fit=True, verbose=True)
rho_est = radon.iradon(sin_err, rot_ctr=rot_ctr)
plot.figure(1)
plot.imshow(rho_est)
plot.draw()

print('Forward projecting the reconstruction')
sin_est = radon.radon(rho_est, sin_act.shape[0], rot_ctr)
plot.figure(2)
plot.imshow(sin_est)
plot.draw()

