#! /opt/local/bin/python2.7 -i

import numpy as np
import matplotlib.pyplot as plot
import radon
import Parzen

n = 256

interface_width = 25.0

dX = n/2.0 + 2.0
dY = n/2.0 - 3.0

nProj = int(round(n*2.0))

# ==============================================================
rho_act = np.zeros((n,n))
for x in range(n):
  for y in range(n):
    t_sqrd = (interface_width)**2
    #
    diam = n*0.40
    cX = n*0.30
    cY = n*0.35
    r_sqrd = (x-cX)**2 + (y-cY)**2.
    R_sqrd = (diam/2.)**2
    #rho_act[x,y] += R_sqrd**0.5 - r_sqrd**0.5
    #rho_act[x,y] += (np.tanh((R_sqrd - r_sqrd)/t_sqrd)+1.0)/2.0
    eps = 0.5
    rho_act[x,y] += R_sqrd**0.5 - r_sqrd**0.5
    if rho_act[x,y] > +eps: rho_act[x,y] = +eps
    if rho_act[x,y] < -eps: rho_act[x,y] = -eps

rho_act = (rho_act+0.5)
# ==============================================================
sin_act = radon.radon(rho_act, nProj)
noise_val = (sin_act.max()-sin_act.min()) * 0.025
noise = np.random.uniform(low=-noise_val,high=+noise_val,size=sin_act.shape)
sin_act = sin_act + noise
# ==============================================================
sin_est = sin_act.copy()
sin_est = Parzen.filter(sin_est, ramp=(0,1))
rho_est = radon.iradon(sin_est)
plot.imshow(rho_est)
# ==============================================================
sin_noisy = Parzen.filter(sin_act+noise, ramp=(0,1))
rho_noisy = radon.iradon(sin_noisy)
plot.imshow(rho_noisy)
# ==============================================================
for i in range(20):
    print "Iteration: %2i"%(i+1)
    sin_est = radon.radon(rho_est, nProj)
    sin_err = sin_act - sin_est
    sin_err = Parzen.filter(sin_err, const=1.0, ramp=(0,0.5), parz=(0,0.1))
    rho_err = radon.iradon(sin_err)
    rho_est += rho_err

plot.imshow(rho_act-rho_est)




import sdf
import smoothing
import curv

noise = 0.05
rho_act  += np.random.uniform(low=-noise,high=+noise,size=rho_act.shape)

#sdf_act = sdf.reinit(rho_act, tol=0.01)

sdf_act = sdf.reinit(rho_act, tol=0.01, band=20., use_weno=True, max_it=250)
#sdf_est = sdf.reinit(n*rho_est-0.5, tol=0.01)

plot.imshow(curv.H(sdf_act, reinit=False), vmin=-2.*2./(0.4*n),vmax=0.0)
plot.imshow(curv.H(sdf_est), vmin=-0.06,vmax=0.0)



plot.clf()
plot.imshow(pws.H(), vmin=-2.*2./(0.4*n),vmax=0.0)
plot.contour(pws.rho, levels=[0], colors='b')
plot.contour(pws.rec, levels=[0], colors='r')
