#! /opt/local/bin/py27-MacPorts -i

def profile(sino, rMin):
    import pstats, cProfile
    from RadonTransform import BackProj
    #
    cProfile.runctx("rho=BackProj(sino, rMin)", 
                    globals(), locals(), "Profile.prof")
    #
    s = pstats.Stats("Profile.prof")
    s.strip_dirs().sort_stats("time").print_stats()

if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plot
    from radon  import iradon
    from DataIO import load_proj, bytscl, write_tiff, read_tiff
    from Parzen import filter
    #from find_ctr import find_center
    # ==============================================================
    sin_act = read_tiff('sin_act.tif')
    #
    sin_act = sin_act.astype(np.float64)
    sin_act = sin_act.T
    sin_act = sin_act/(2.0**16-1)
    sin_act = -np.log(sin_act)
    # Filter the sinogram:
    sin_est = filter(sin_act, parz=(0.0,0.0), ramp=(0.0,1.0))
    # ==============================================================
    rMin = find_center(sin_est, kMin=-1, kMax=3, drawPlots=True)
    # ==============================================================
    profile(sin_est, rMin)
    rho=BackProj(sin_est, rMin)
    #
    plot.figure(0, figsize=(8,8))
    plot.clf()
    plot.imshow(rho)
    plot.draw()
    #
    h=np.histogram(rho[150:-150,150:-150],bins=256)
    plot.figure()
    plot.plot(h[1][0:-1],h[0], '-')
    plot.draw()
    #
    plot.show()

