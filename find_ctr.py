#! /opt/local/bin/python2.7 -i

def find_center(sino, est=None, kMin=-2, kMax=4, drawPlots=False):
    import numpy as np
    from scipy.ndimage import zoom
    import matplotlib.pyplot as plot
    #
    from RadonTransform import BackProj
    #
    nT, nR = sino.shape
    if est == None: rMin = nR/2.
    else: rMin = est
    #
    for k in range(kMin, kMax+1):
        rMinVals = []
        q_iaVals = []
        #=======================================================
        # Downsample the sinogram to speed things up:
        nR_sz = min(2.0**k, 1.0)
        dR_sz = 2.0**-k
        sino_small = zoom(sino, (1.0,nR_sz))
        nT, nR = sino_small.shape
        #=======================================================
        print "========================================"
        print " Tomograph resolution: %i angles x %i pixels"%(nT, nR)
        print " rMin search resolution: %4.2f pixels\n"%(dR_sz)
        print "      rMin      Q_ia"
        print "   =======  ========="
        #=======================================================
        m0 = np.mean(np.abs(sino_small))/nR
        #
        if dR_sz >  1.0: dR_range  = np.arange(-4.0,5.0)
        if dR_sz <= 1.0: dR_range  = np.arange(-2.0,3.0)
        #dR_range  = np.arange(-20.0,21.0)
        for dr in dR_range:
            #rMin_est = (rMin*nR_sz + dr*dR_sz)
            rMin_est = (rMin + dr*dR_sz)*nR_sz
            rho = BackProj(sino_small, rMin_est)
            q_ia = -np.mean(np.abs(rho))/m0
            #
            print "   % 7.2f  %9.6f" % (rMin_est, q_ia)
            #
            rMinVals.append(rMin_est)
            q_iaVals.append(q_ia)
            #
            #fn = '/scratch/find_ctr_%04i.tif'%rMin_est
            #rho=BackProj(sino_small, rMin*nR_sz)
            #write_tiff(fn, rho)
        #=======================================================
        min_val = min(q_iaVals)
        min_loc = q_iaVals.index(min_val)
        rMin = rMinVals[min_loc]/nR_sz
        print "\n rMin = % 7.2f   % 7.2f" % (rMinVals[min_loc], rMin)
        #=======================================================
        if drawPlots == True:
            rho=BackProj(sino_small, rMin*nR_sz)
            #
            plot.figure(1, figsize=(8,8))
            plot.clf()
            plot.imshow(rho)
            plot.draw()
            #
            plot.figure(2, figsize=(8,8))
            plot.clf()
            plot.plot(rMinVals, q_iaVals, 'ko')
            plot.plot(rMinVals[min_loc], q_iaVals[min_loc], 'ro', markersize=15)
            plot.draw()
        #
    #
    return rMin

# ==============================================================
if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plot
    from RadonTransform import BackProj
    from DataIO         import load_proj, bytscl, write_tiff, read_tiff
    from Parzen         import filter
    # ==============================================================
    sino = read_tiff('sinogram.tif')
    #
    sino = sino.astype(np.float64)
    sino = sino.T
    sino = sino/2.0**16
    sino = -np.log(sino)
    # Filter the sinogram:
    sino = filter(sino, parz=(0.0,0.005), ramp=(0.0,1.0))
    # ==============================================================
    rMin = find_center(sino, kMin=-2, kMax=3, drawPlots=True)
    # ==============================================================
    rho=BackProj(sino, rMin)
    #
    plot.figure(0, figsize=(8,8))
    plot.clf()
    plot.imshow(rho)
    plot.draw()
    #
    h=np.histogram(rho,bins=256)
    plot.figure()
    plot.plot(h[1][0:-1],h[0], '-')
    plot.draw()
    #
    plot.show()

