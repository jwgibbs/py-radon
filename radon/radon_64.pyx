#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: embedsignature=True

import numpy as np
from radon_math cimport sqrt, fabs, cos, sin, floor
from cython.parallel cimport prange
# ==============================================================
def radon_64(double[:,:,::1] arr, double[:] angles, double rot_ctr):
    """
    
    """
    # Typed variables:
    cdef  ssize_t  x, nx=arr.shape[0]
    cdef  ssize_t  y, ny=arr.shape[1]
    cdef  ssize_t  z, nz=arr.shape[2]
    cdef  ssize_t  a, na=angles.shape[0]
    cdef  ssize_t  r, nr=max(nx, ny)
    cdef  double  angle
    cdef  double  x_cart, y_cart, r_cart, a_cart
    cdef  double  x_orig, y_orig, r_orig, a_orig
    cdef  double  cos_angle, sin_angle
    cdef  double  x_lo, x_hi, y_lo, y_hi
    cdef  double  x_real, y_real, w_lo, w_hi
    cdef  double  pi=3.14159265358979323846
    # Typed arrays:
    cdef  double[:,:,::1]  out=np.zeros((na,nr,nz), dtype=np.float64)
    # ==========================================================
    #
    # - x_iter,y and r_iter a are integer iterators in
    #   array coordinates (i.e. they have zeros at the start
    #   of the array)
    # - x_cart,y_cart and r_cart are the in Cartesian
    #   coordinates and have zeros at the center of rotation
    # - similarly, a_iter is the integer angle and
    #   a_cart is the actual angle with a range of (0,pi)
    #
    # Variable 'origin'.  i.e. the index that corresponds to
    # zero in the cartesian coordinates:
    x_orig = (<double> nx - 1.0) / 2.0
    y_orig = (<double> ny - 1.0) / 2.0
    z_orig = 0.0
    r_orig = rot_ctr
    t_orig = 0.0
    #
    with nogil:
      for a in prange(na, schedule='guided'):
        angle = angles[a] * pi / 180.0
        sin_angle = sin(angle)
        cos_angle = cos(angle)
        for r in range(nr):
          r_cart = <double> r - r_orig
          if fabs(sin_angle) < 0.5:
            for x in range(nx):
              x_cart = <double> x - x_orig
              y_cart = (r_cart - x_cart * sin_angle) / cos_angle
              if (x_cart / <double> nx)**2 + (y_cart / <double> ny)**2 <= 1.0:
                y_real = y_orig + y_cart
                #
                y_lo = floor(y_real)
                y_hi = y_lo + 1.0
                #
                w_lo = - y_real + y_hi
                w_hi = + y_real - y_lo
                #
                if (y_lo >= 0) and (y_lo < ny):
                  for z in range(nz):
                    out[a, r, z] += 1.0/fabs(cos_angle) * w_lo * arr[x, <ssize_t> y_lo, z]
                if (y_hi >= 0) and (y_hi < ny):
                  for z in range(nz):
                    out[a, r, z] += 1.0/fabs(cos_angle) * w_hi * arr[x, <ssize_t> y_hi, z]
          else:
            for y in range(ny):
              y_cart = <double> y - y_orig
              x_cart = (r_cart - y_cart * cos_angle) / sin_angle
              if (x_cart / <double> nx)**2 + (y_cart / <double> ny)**2 <= 1.0:
                x_real = x_orig + x_cart
                #
                x_lo = floor(x_real)
                x_hi = x_lo + 1.0
                #
                w_lo = - x_real + x_hi
                w_hi = + x_real - x_lo
                #
                if (x_lo >= 0) and (x_lo < nx):
                  for z in range(nz):
                    out[a, r, z] += 1.0/fabs(sin_angle) * w_lo * arr[<ssize_t> x_lo, y, z]
                if (x_hi >=0) and (x_hi <nx):
                  for z in range(nz):
                    out[a, r, z] += 1.0/fabs(sin_angle) * w_hi * arr[<ssize_t> x_hi, y ,z]
      #
      for a in prange(na):
        for r in range(nr):
          for z in range(nz):
            out[a, r, z] = out[a, r, z] / (<double> nr)
    #
    return np.asarray(out, dtype=np.float64)
