#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: embedsignature=True

import numpy as np
from radon_math cimport sqrtf, fabsf, f_max, f_min, f_sign
from radon_math cimport sqrt, fabs, d_max, d_min, d_sign
from cython.parallel cimport prange
#=== 32-bit Parzen filter ======================================================
def parz_32(float[:,::1] mask, float freq):
    cdef  ssize_t  x=0, nx=mask.shape[0]
    cdef  ssize_t  y=0, ny=mask.shape[1]
    cdef  float  dist=0.0, cutoff=0.0
    #
    cutoff = freq * ny
    with nogil:
      for x in prange(nx):
        for y in range(ny):
            # ======================================================
            dist = f_min((<float> y + 0.5), (<float> ny - <float> y - 0.5)) / cutoff
            #
            if   (dist < 0.5):
                mask[x,y] = mask[x,y] * 1 + 0*dist - 6*dist**2 + 6*dist**3
            elif (dist < 1.0):
                mask[x,y] = mask[x,y] * 2 - 6*dist + 6*dist**2 - 2*dist**3
            else:
                mask[x,y] = mask[x,y] * 0.
    # ==========================================================
    return np.asarray(mask, dtype=np.float32)

#=== 64-bit Parzen filter ======================================================
def parz_64(double[:,::1] mask, double freq):
    cdef  ssize_t  x=0, nx=mask.shape[0]
    cdef  ssize_t  y=0, ny=mask.shape[1]
    cdef  double  dist=0.0, cutoff=0.0
    #
    cutoff = freq * ny
    with nogil:
      for x in prange(nx):
        for y in range(ny):
            # ======================================================
            dist = d_min((<double> y + 0.5), (<double> ny - <double> y - 0.5)) / cutoff
            #
            if   (dist < 0.5):
                mask[x,y] = mask[x,y] * 1 + 0*dist - 6*dist**2 + 6*dist**3
            elif (dist < 1.0):
                mask[x,y] = mask[x,y] * 2 - 6*dist + 6*dist**2 - 2*dist**3
            else:
                mask[x,y] = mask[x,y] * 0.
    # ==========================================================
    return np.asarray(mask, dtype=np.float64)

#=== 32-bit ramp filter ========================================================
def ramp_32(float[:,::1] mask, float ramp):
    cdef  ssize_t  x=0, nx=mask.shape[0]
    cdef  ssize_t  y=0, ny=mask.shape[1]
    cdef  float  dist=0.0
    #
    with nogil:
      for x in prange(nx):
        for y in range(ny):
          dist = f_min((<float> y + 0.5), (<float> ny - <float> y - 0.5))
          mask[x,y] = mask[x,y] * dist * ramp
    # ==========================================================
    return np.asarray(mask, dtype=np.float32)

#=== 64-bit ramp filter ========================================================
def ramp_64(double[:,::1] mask, double ramp):
    cdef  ssize_t  x=0, nx=mask.shape[0]
    cdef  ssize_t  y=0, ny=mask.shape[1]
    cdef  double  dist=0.0
    #
    with nogil:
      for x in prange(nx):
        for y in range(ny):
          dist = d_min((<double> y + 0.5), (<double> ny - <double> y - 0.5))
          mask[x,y] = mask[x,y] * dist * ramp
    # ==========================================================
    return np.asarray(mask, dtype=np.float64)

