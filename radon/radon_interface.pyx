def radon(arr, angles=181, rot_ctr=None):
    """
    DESCRIPTION
    ===========
        Performs a Radon transform (in the forward direction) of
        the input data.
    
    INPUTS
    ======
        arr ----- 2D or 3D Numpy array; required
                  The data to be transformed. Data is rotated 
                  about the 3rd dimension (only applicable for
                  3D datasets).
        
        angles -- float, list or Numpy array; optional
                  Angles at which the Radon transform is 
                  computed. If a float is provided, this 
                  specifies the number of angles and uniform 
                  spacing between 0 and 180 degrees (inclusive) 
                  is used. If a list or an array is provided, 
                  the values specify the exact angles. Units are
                  degrees.
        
        rot_ctr - float; optional
                  Center of rotation as it is projected onto the
                  detector. Defaults to the center of the data.
        
    RETURNS
    =======
        out ----- Numpy array
                  The transformed data. Dimensions are (n, n, z)
                  where n and z are specified by:
                      x, y, z = arr.shape
                      n = max(x, y)
    
    USEAGE
    ======
        >>> sinogram = load_data(filename)
        >>> filtered = radon.filter(sinogram, ramp=1, parz=100)
        >>> angles = [0, 30, 45, 60, 90, 180]
        >>> reconstruction = radon.iradon(filtered, angles)
    
    NOTES
    =====
        * The algorithm used is back projection using linear 
          interpolation.
        * 
      
    """
    ndim = arr.ndim
    if ndim not in [2, 3]:
        raise ValueError('2D or 3D data required')
    # 
    import numpy as np
    import fwd_radon
    # Make the array 3D and C-aligned so that it conforms to
    # the radon() input format
    arr = np.atleast_3d(arr)
    arr = np.require(arr,  dtype=np.float64, requirements=['C', 'ALIGNED'])
    # Fine the center of rotation if none is provided:
    nx, ny, nz = arr.shape
    nr = max(nx, ny)
    if rot_ctr == None:
        rot_ctr = (float(nr) - 1.) / 2.
    else:
        rot_ctr = float(rot_ctr)
    # Make the list of angles:
    if isinstance(angles, int) or isinstance(angles, float):
        angles = np.linspace(0.0, 180.0, angles)
    elif isinstance(angles, tuple) or isinstance(angles, list) or isinstance(angles, np.array):
        angles = np.asarray(angles, dtype=np.float64)
    else:
        raise ValueError('angles could not be converted into a float or numpy array')
    # Perform the radon transform:
    out = fwd_radon.radon(arr, angles, rot_ctr)
    # Return the result with the same number of dimensions 
    # as the input:
    if ndim == 2:
        out = out[:,:,0]
    return out

