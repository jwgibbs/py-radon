import numpy as np
from scipy.fftpack import rfft, irfft
from filters import parz_32, parz_64, ramp_32, ramp_64

def filter(arr, ramp=1.0, parz=None, constant=None, inplace=False):
    """
    Filters along the second axis only
    
    arr axis ordering:
        1) angle
        2) perpendicular to the axis of rotation
        3) parallel to the axis of rotation
    
    INPUTS
    ======
        arr ----- 
                  
        
        ramp ---- float; optional
                  
        
        parz ---- float; optional
                  Cutoff distance for Parzen filtering as a 
                  fraction of the size of the input array
        
        const --- float; optional
                  A constant that is added to the mask after
                  the other filtering options
        
        inplace - boolean, optional
                  If True, the array is modified inplace, saving memory but
                  overwriting the contents of the input array.
        
    """
    #=== check inputs ==========================================================
    if (ramp != None) and (ramp < 0):
        raise ValueError('ramp must be a positive number')
    if (parz != None) and (parz < 0):
        raise ValueError('parz must be a positive number')
    #=== setup =================================================================
    dtype = arr.dtype
    ndim = arr.ndim
    if ndim == 2:
        nx, ny = arr.shape
    elif ndim == 3:
        nx, ny, nz = arr.shape
    else:
        raise ValueError('arr must be 2D or 3D')
    mask = np.ones((nx, ny), dtype=dtype)
    if inplace == True:
        working_arr = arr
    else:
        working_arr = arr.copy()
    #=== generate the filter ===================================================
    if ramp != None:
        if dtype == np.float32:
            mask = ramp_32(mask, ramp)
        else:
            mask = ramp_64(mask, ramp)
    if parz != None:
        if dtype == np.float32:
            mask = parz_32(mask, parz)
        else:
            mask = parz_64(mask, parz)
    if constant != None:
        mask = mask + constant
    #=== apply the filter ======================================================
    if ndim == 2:
        result = irfft(mask * rfft(working_arr, axis=1, overwrite_x=True), axis=1, overwrite_x=True).real
    else:
        mask = np.repeat(np.atleast_3d(mask), nz, axis=2)
        result = irfft(mask * rfft(working_arr, axis=1, overwrite_x=True), axis=1, overwrite_x=True).real
    #
    return result
    