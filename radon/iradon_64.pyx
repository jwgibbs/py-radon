#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: embedsignature=True

import numpy as np
from radon_math cimport sqrt, fabs, cos, sin, floor
from cython.parallel cimport prange
# ==============================================================
def iradon_64(double[:,:,::1] arr, double[:] angles, double rot_ctr):
    # Type variables:
    cdef  ssize_t  a, na=arr.shape[0]
    cdef  ssize_t  r, nr=arr.shape[1]
    cdef  ssize_t  z, nz=arr.shape[2]
    cdef  ssize_t  x, nx=nr
    cdef  ssize_t  y, ny=nr
    cdef  double  angle
    cdef  double  x_cart, y_cart, r_cart, a_cart
    cdef  double  x_orig, y_orig, r_orig, a_orig
    cdef  double  cos_angle, sin_angle
    cdef  double  C1, C2
    cdef  double  r_real, r_lo, r_hi, w_lo, w_hi
    cdef  double  pi=3.14159265358979323846
    # Typed arrays:
    cdef  double[:,:,::1]  out=np.zeros((nx,ny,nz), dtype=np.float64)
    # Variable 'origin'.  i.e. the index that corresponds to
    # zero in the cartesian coordinates:
    x_orig = (<double> nx - 1.0) / 2.0
    y_orig = (<double> ny - 1.0) / 2.0
    z_orig = 0.0
    r_orig = rot_ctr
    t_orig = 0.0
    #===========================================================
    with nogil:
      for a in range(na):
        angle = angles[a] * pi / 180.0
        cos_angle = cos(angle)
        sin_angle = sin(angle)
        for x in prange(nx, schedule='static'):
          x_cart = x - x_orig
          for y in range(ny):
            y_cart = y - y_orig
            r_cart = x_cart * sin_angle + y_cart * cos_angle
            #
            r_real = r_orig + r_cart
            #
            r_lo = floor(r_real)
            r_hi = r_lo + 1.0
            #
            w_lo = r_hi - r_real
            w_hi = r_real - r_lo
            # find weighting factors
            if (r_lo >= 0) and (r_lo < nr):
              for z in range(nz):
                out[x, y, z] += w_lo * arr[a, <ssize_t> r_lo, z]
            if (r_hi >= 0) and (r_hi < nr): 
              for z in range(nz):
                out[x, y, z] += w_hi * arr[a, <ssize_t> r_hi, z]
      #
      for x in prange(nx, schedule='static'):
        for y in range(ny):
          for z in range(nz):
            out[x, y, z] = out[x, y, z] / (<double> na)
    #
    return np.asarray(out, dtype=np.float64)
