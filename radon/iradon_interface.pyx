import numpy as np
from radon.iradon_32 import iradon_32
from radon.iradon_64 import iradon_64

def iradon(arr, angles=None, rot_ctr=None):
    """
    DESCRIPTION
    ===========
        Performs the inverse Radon transform on the input data.
    
    INPUTS
    ======
        arr ----- 2D or 3D Numpy array; required
                  The data to be transformed. Data axis order
                  must be:
                  1) angular dimension
                  2) perpendicular to the axis of rotation
                  3) parallel to the axis of rotation (optional)
        
        angles -- None, list or Numpy array; optional
                  Angles at which the Radon transform is 
                  computed. If a list or an array is provided, 
                  the values specify the exact angles. 
                  Units are degrees. Default is to use the 
                  number of angles in arr (arr.shape[0]), with
                  uniform spacing between 0 and 180 degrees.
        
        rot_ctr - float; optional
                  Center of rotation as it is projected onto the
                  detector. Defaults to the center of the data.
        
    RETURNS
    =======
        out ----- Numpy array
                  The transformed data. 
    
    USEAGE
    ======
        
    NOTES
    =====
        * Units for angles are degrees
    
    """
    dtype = arr.dtype
    ndim = arr.ndim
    if ndim not in [2, 3]:
        raise ValueError('2D or 3D data required')
    # Make the array 3D, C-aligned so that it conforms to the iradon input format
    arr = np.atleast_3d(arr)
    if arr.flags['C_CONTIGUOUS'] == False:
        arr = np.require(arr, dtype=dtype, requirements=['C', 'ALIGNED'])
    # Find the center of rotation if none is provided:
    na, nr, nz = arr.shape
    nx = nr
    ny = nr
    if rot_ctr == None:
        rot_ctr = (float(nr) - 1.) / 2.
    else:
        rot_ctr = float(rot_ctr)
    # Make the list of angles:
    if angles == None:
        angles = np.linspace(0.0, 180.0, na).astype(dtype)
    else:
        angles = np.array(angles, dtype=dtype)
        if angles.shape[0] != arr.shape[0]:
            raise ValueError('the number of entries in angles must match the first dimesnion in arr')
    # Perform the radon transform:
    if dtype == np.float32:
        out = iradon_32(arr, angles, rot_ctr)
    else:
        out = iradon_64(arr, angles, rot_ctr)
    # Return the result with the same number of dimensions as the input:
    if ndim == 2:
        out = out[:,:,0]
    return out

