from __future__ import print_function
import radon
import scipy.ndimage
import numpy as np

def find_rotctr(sinogram, guess=None, span=None, nvals=5, multi_res=1, quad_fit=False, full_output=False, verbose=False):
    """
    Determines the optimal center of rotation for tomography data. This is done
    by 
    
    Inputs
    ======
        sinogram : 2D numpy array
            Index order: angles, detector pixel number
        
        guess : float, optional
            Initial guess for the center of rotation.
            Defaults to the mid-point of the sinogram.
        
        span : float, optional
            The range of rotation center values that are tested. The span is the
            total range of values used, so guess = 50 and span = 10 will result
            in values between 45 and 55 being tested.
            Defaults to 10% of the array size.
        
        nvals : int, optional
            The number of rotation center values to test.
            Defaults to 5.
        
        multires : int, optional
            Number of multi-resolution stages to do. For each multi-resolution 
            stage, the data is downsampled by a factor of 2. The span is not 
            scaled, which makes this is useful if the initial guess for the 
            center of rotation isn't very good. 
            
            For example, if the sinogram has 
            100 pixels across, span is set to 10 pixels and 3 multi-resolution 
            stages are used (for zoom factors of 0.25, 0.50, and 1.00), the 
            initial step will scale the array by a factor of 0.25, and center of 
            rotation values of 12.5 +/- 10 will be tested. This makes it 
            possible to search almost the entire range of possibilities but the 
            final scan will still be focused on the range of +/-10 about the 
            best point from the previous multi-res step.
            
            This is probably of limited use if the rotation center is relatively
            well known.
        
        quad_fit : bool, optional
            Flag for whether or not to fit a quadratic and find the minimum of 
            that. If True, the quadratic fit will be performed and the argmin
            location will be tested without any special treatment relative to 
            the fixed-grid points. Therefore, if the fit is poor, the algorithm 
            will fall back on one of the better points defined by guess, span 
            and nvals.
        
    Outputs
    =======
        
        
    """
    if guess == None:
        guess = (0.50) * sinogram.shape[1]
    
    RC_best = guess
    
    if span == None:
        span = (0.10) * sinogram.shape[1]
    
    mres_stages = np.logspace(1-multi_res, 0, num=multi_res, base=2)
    for zoom_factor in mres_stages:
        if verbose:
            print('\n' + 'Starting multi-resolution stage at %.3f scaling' % zoom_factor)
            print('  ' + '+------------+------------+')
            print('  ' + '|    rot ctr |    entropy |')
            print('  ' + '+------------+------------+')
        
        sino_zoom = scipy.ndimage.zoom(sinogram, (1.0, zoom_factor))
        
        rot_ctr_min = RC_best * zoom_factor - (span / 2.0)
        rot_ctr_max = RC_best * zoom_factor + (span / 2.0)
        
        rot_ctr = list(np.linspace(rot_ctr_min, rot_ctr_max, nvals))
        entropy = []
        
        for RC_val in rot_ctr:
            rho_est = radon.iradon(sino_zoom, rot_ctr=RC_val)
            SE_val = shannon_entropy(rho_est, min=-5.0, max=+10.0, nbins=256)
            entropy.append(SE_val)
            if verbose: print('  ' + '| %10.3f | %10.4f |' % (RC_val/zoom_factor, SE_val))
        
        if quad_fit == True:
            fit_coef = np.polyfit(rot_ctr, entropy, 2)
            fit_func = np.poly1d(fit_coef)
            fit_vals = fit_func(entropy)
            
            a, b, c = fit_coef
            RC_val = (-b) / (2 * a)
            
            rho_est = radon.iradon(sino_zoom, rot_ctr=RC_val)
            SE_val = shannon_entropy(rho_est, min=-5.0, max=+10.0, nbins=256)
            rot_ctr.append(RC_val)
            entropy.append(SE_val)
            if verbose: print('  ' + '| %10.3f | %10.4f |' % (RC_val/zoom_factor, SE_val))
        
        RC_best = rot_ctr[np.argmin(entropy)] / zoom_factor
        
        if verbose:
            print('  ' + '+------------+------------+')
            print('Best value: %.3f' % RC_best)
        
    return RC_best

def shannon_entropy(arr, min=None, max=None, nbins=256):
    """
    Shannon image entropy of an array.
    
    SE = - sum( (hist * ln(hist) )
    
    Inputs
    ======
    
    Outputs
    =======
    """
    import numpy as np
    if min == None:
        min = arr.min()
    if max == None:
        max = arr.max()
    val, loc = np.histogram(arr, bins=nbins, range=(min, max), normed=False)
    val = val.astype('float64')
    entropy = -np.sum(val/np.sum(val) * np.log(val/np.sum(val) + 1e-9))
    return entropy

