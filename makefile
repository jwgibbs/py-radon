PYTHON=/opt/local/bin/py27-MacPorts
CYTHON=/opt/local/bin/cython-2.7

CC=/usr/local/bin/gcc
CC_OPT=-O3 -fPIC -fopenmp
INCLUDE_DIRS=-I/opt/local/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7

LINK_DIRS=-L/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib
LINK_LIBS=-lpython2.7
LINK_OPTS=-shared -fopenmp

all: manual

distutils:
	$(PYTHON) setup/setup.py build_ext --inplace

manual: fwd_radon.so inv_radon.so


fwd_radon.so:
	#################### radon ####################
	mkdir -p build
	### Cython:
	$(CYTHON) -a radon/fwd_radon.pyx -o build/fwd_radon.c
	### Compile:
	$(CC) $(INCLUDE_DIRS) $(CC_OPT) -c build/fwd_radon.c -o build/fwd_radon.o
	### Link:
	$(CC) $(LINK_DIRS) $(LINK_LIBS) $(LINK_OPTS) build/fwd_radon.o -o fwd_radon.so

inv_radon.so:
	#################### inverse radon ####################
	mkdir -p build
	### Cython:
	$(CYTHON) -a radon/inv_radon.pyx -o build/inv_radon.c
	### Compile:
	$(CC) $(INCLUDE_DIRS) $(CC_OPT) -c build/inv_radon.c -o build/inv_radon.o
	### Link:
	$(CC) $(LINK_DIRS) $(LINK_LIBS) $(LINK_OPTS) build/inv_radon.o -o inv_radon.so

install:
	mv fwd_radon.so /usr/local/JWG/bin/
	mv inv_radon.so /usr/local/JWG/bin/
	mv radon.so /usr/local/JWG/bin/

clean:
	rm -rf *.so
	rm -rf build/*
	rm -rf radon/*.c
	rm -rf radon/*.html
